﻿using Employee_CaseStudy.Models;

namespace Employee_CaseStudy.BL
{
    public interface IEmployee
    {
        public List<Employee> GetAllEmployeeData();
        public Employee GetEmployeebyByID(int employeebyID);

        public bool InsertnewEmployee(Employee employee);

        public bool UpdateEmployee(Employee employee);

        public  bool DeleteEmployee(int employeeID);

        public List<Employee> SearchFilterofEmployee(EmployeeSearchFilter searchFilter);
    }
}
