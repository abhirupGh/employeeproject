﻿using Employee_CaseStudy.Data;
using Employee_CaseStudy.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Employee_CaseStudy.BL
{
    public class EmployeeDataFetch : IEmployee
    {
        private readonly ApplicationDbContext _db;
        public EmployeeDataFetch(ApplicationDbContext db)
        {
            this._db = db;
        }


        public bool DeleteEmployee(int employeeID)
        {
            if (employeeID <= 0)
            {
                return false;
            }
            var empDetails = GetEmployeebyByID(employeeID);
            if (empDetails.managerid >= 0)
            {
                this._db.Tbl_Employee.Remove(empDetails);
                this._db.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Employee> GetAllEmployeeData()
        {
            var allemploye = this._db.Tbl_Employee.Include("Department").ToList();

            if (allemploye.Count > 0)
            {
                allemploye.Where(ass => allemploye.Any(x => x.managerid == ass.emp_id)).
                    ToList().ForEach(x => x.Status = "Manager");

                allemploye.Where(x => x.managerid == null).ToList().ForEach(action => action.Status = "Head");
            }
            return allemploye;
        }

        public Employee GetEmployeebyByID(int employeebyID)
        {
            return this._db.Tbl_Employee.First(
                emp => emp.emp_id == employeebyID
            );
        }

        public bool InsertnewEmployee(Employee employee)
        {
            if (employee == null)
            {
                return false;
            }

            this._db.Add(employee);
            this._db.SaveChanges();
            return true;
        }

        public List<Employee> SearchFilterofEmployee(EmployeeSearchFilter searchFilter)
        {
            throw new NotImplementedException();
        }

        public bool UpdateEmployee(Employee employee)
        {
            if (employee == null || employee.emp_id <= 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(employee.lname))       /* From UI We only allow to update Email & First Name*/
            {
                var empDetails = GetEmployeebyByID(employee.emp_id);
                empDetails.emailid = employee.emailid;
                empDetails.fname = employee.fname;
                this._db.Update(empDetails);
            }
            else
                this._db.Update(employee);

            this._db.SaveChanges();
            return true;
        }
    }
}
