﻿using Employee_CaseStudy.Models;
using Microsoft.EntityFrameworkCore;

namespace Employee_CaseStudy.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        /*protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                        .HasOne(c => c.Manager)
                        .WithMany()
                        .HasForeignKey(c => c.managerid);
        }*/

        public DbSet<Employee> Tbl_Employee { get; set; }
        public DbSet<Department> Tbl_Department { get; set; }
    }
}
