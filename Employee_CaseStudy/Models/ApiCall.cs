﻿using Newtonsoft.Json;
using System.Text;

namespace Employee_CaseStudy.Models
{
    public static class ApiCall
    {
        private static readonly string URl = "https://localhost:7015/api/emp/{0}";

        public async static Task<string> GetApiCall(string apiName)
        {
            using (var httpClinet = new HttpClient())
            {
                var response = await httpClinet.GetAsync(string.Format(URl, apiName));
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }

        public async static Task<string> PostApiCall(string apiName, string param)
        {
            using (var httpClinet = new HttpClient())
            {
                var stringContent = new StringContent(param, Encoding.UTF8, "application/json");
                var response = await httpClinet.PostAsync(string.Format(URl, apiName), stringContent);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
    }

}