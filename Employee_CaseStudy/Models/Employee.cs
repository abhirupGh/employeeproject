﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Employee_CaseStudy.Models
{
    [Table("tbl_Employee")]
    public class Employee
    {
        private string? status;
        [Key]
        public int emp_id { get; set; }
        [Required]
        public string fname { get; set; }
        public string lname { get; set; }
        [Required]
        public string emailid { get; set; }
        public int departmentid { get; set; }
        [ForeignKey("departmentid")]
        public Department Department { get; set; }  //Navigation Property
        public int? managerid { get; set; }
        /*public Employee Manager { get; set; }   //Navigation Property*/

        [NotMapped]
        public string Status
        {

            get { return string.IsNullOrEmpty(status) ? "Assoc" : status; }
            set
            {
                this.status = value;
            }
        }

    }

    public class EmployeeSearchFilter
    {
        public int emp_id { get; set; }
        public string fname { get; set; }
        public string emailid { get; set; }
    }

}
