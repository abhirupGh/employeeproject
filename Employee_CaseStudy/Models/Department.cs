﻿using System.ComponentModel.DataAnnotations;
namespace Employee_CaseStudy.Models
{
    public class Department
    {
        public int departmentid { get; set; }
        [Required]
        public string departmentname { get; set; }
    }
}
