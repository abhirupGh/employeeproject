﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Employee_CaseStudy.Migrations
{
    public partial class Codefirst_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tbl_Employee_Tbl_Department_departmentid",
                table: "Tbl_Employee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tbl_Employee",
                table: "Tbl_Employee");

            migrationBuilder.RenameTable(
                name: "Tbl_Employee",
                newName: "tbl_Employee");

            migrationBuilder.RenameIndex(
                name: "IX_Tbl_Employee_departmentid",
                table: "tbl_Employee",
                newName: "IX_tbl_Employee_departmentid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tbl_Employee",
                table: "tbl_Employee",
                column: "emp_id");

            migrationBuilder.AddForeignKey(
                name: "FK_tbl_Employee_Tbl_Department_departmentid",
                table: "tbl_Employee",
                column: "departmentid",
                principalTable: "Tbl_Department",
                principalColumn: "departmentid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbl_Employee_Tbl_Department_departmentid",
                table: "tbl_Employee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tbl_Employee",
                table: "tbl_Employee");

            migrationBuilder.RenameTable(
                name: "tbl_Employee",
                newName: "Tbl_Employee");

            migrationBuilder.RenameIndex(
                name: "IX_tbl_Employee_departmentid",
                table: "Tbl_Employee",
                newName: "IX_Tbl_Employee_departmentid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tbl_Employee",
                table: "Tbl_Employee",
                column: "emp_id");

            migrationBuilder.AddForeignKey(
                name: "FK_Tbl_Employee_Tbl_Department_departmentid",
                table: "Tbl_Employee",
                column: "departmentid",
                principalTable: "Tbl_Department",
                principalColumn: "departmentid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
