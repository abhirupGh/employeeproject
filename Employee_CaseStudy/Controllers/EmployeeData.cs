﻿using Employee_CaseStudy.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace Employee_CaseStudy.Controllers
{
    public class EmployeeData : Controller
    {

        public async Task<ActionResult> EmployeeList()
        {
            List<Employee> employeeData = new();
            var result = await ApiCall.GetApiCall("GetAllEmployee");
            employeeData = JsonConvert.DeserializeObject<List<Employee>>(result);
            return View(employeeData);
        }

        public async Task<ActionResult> EmployeeDetail(int empID)
        {
            Employee employeeData = new();

            var result = await ApiCall.GetApiCall("GetEmployeeDatabyID?empid=" + empID);

            if (!string.IsNullOrEmpty(result))
            {
                employeeData = JsonConvert.DeserializeObject<Employee>(result);
            }
            return PartialView("_EmployeeDetails", employeeData);


        }

        public string RemoveEmployee(int empID)
        {
            var result = ApiCall.PostApiCall("DeleteEmployee", JsonConvert.SerializeObject(empID));
            if (!string.IsNullOrEmpty(result.Result))
            {
                if (result.Result.ToLower() == "true")
                {
                    return "Success";
                }
            }
            return "Sorry | You Can't Delete Boss Data";
        }

        [HttpPost]
        public string UpdateEmployee(Employee emp)
        {
            var result = ApiCall.PostApiCall("UpdateEmployeeData", JsonConvert.SerializeObject(emp));
            if (!string.IsNullOrEmpty(result.Result))
            {
                if (result.Result.ToLower() == "true")
                {
                    return "Success";
                }
            }
            return string.Empty;
        }
    }
}
;