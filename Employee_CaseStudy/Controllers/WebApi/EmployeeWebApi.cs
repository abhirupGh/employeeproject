﻿using Employee_CaseStudy.BL;
using Employee_CaseStudy.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Employee_CaseStudy.Controllers.WebApi
{
    [ApiController]
    public class EmployeeWebApi : ControllerBase
    {
        private readonly IEmployee _emp;

        public EmployeeWebApi(IEmployee emp)
        {
            this._emp = emp;
        }

        [HttpGet]
        [Route("api/emp/CheckConnection")]
        public string CheckConnection()
        {
            return "Connected";
        }

        [HttpGet]
        [Route("api/emp/GetAllEmployee")]
        public IActionResult GetAllStudent()
        {
            return Ok(this._emp.GetAllEmployeeData());
        }

        [HttpGet]
        [Route("api/emp/GetEmployeeDatabyID")]
        public IActionResult GetEmployeeDatabyID(int empid)
        {
            return Ok(this._emp.GetEmployeebyByID(empid));
        }

        [HttpPost]
        [Route("api/emp/InsertNewEmployee")]
        public IActionResult InsertNewEmployee(Employee emp)
        {
            return Ok(this._emp.InsertnewEmployee(emp));
        }

        [HttpPost]
        [Route("api/emp/UpdateEmployeeData")]
        public IActionResult UpdateEmployeeData(EmployeeSearchFilter emp)
        {
            Employee empdetails = new Employee()
            {
                emp_id = emp.emp_id,
                emailid = emp.emailid,
                fname = emp.fname
            };
            return Ok(this._emp.UpdateEmployee(empdetails));
        }

        [HttpPost, Route("api/emp/DeleteEmployee")]
        public IActionResult DeleteEmployee([FromBody] int empid)
        {
            return Ok(this._emp.DeleteEmployee(empid));
        }

    }
}
